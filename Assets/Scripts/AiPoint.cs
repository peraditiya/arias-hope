﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    [RequireComponent(typeof(BoxCollider2D))]

    public class AiPoint : MonoBehaviour {

        public static int TRIGGER_WALL = 0;
        public static int TRIGGER_FALL = 1;

        public Vector3 direction;
        public int trigger;

        // Use this for initialization
        void Start() {

        }
    }
}