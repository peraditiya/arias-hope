﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Prolouge : MonoBehaviour {

		// Use this for initialization

		private Body _parallax; 

		public float scrollSpeed = 0.01f;

		void Awake () {
			
			//Root.viewport.background = new Body(GameObject.Find("Background_00"));
			Root.viewport.parallax = null;
			Root.viewport.background = null;
			this._parallax = new Body(GameObject.Find("Background_04"));
		}

		void Start () {
			Root.viewport.isXAxisLocked = true;
			Root.player.speed = new Vector3(0.05f, 0.1f, 0);
			Stage.currentRoom.transform.Find("Ground").GetComponent<Moving>().body = Stage.currentRoom.body;
		}
		
		// Update is called once per frame
		void FixedUpdate () {
			
			this._parallax.position -= new Vector3(scrollSpeed, 0, 0);
		}
	}
}