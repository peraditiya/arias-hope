using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
	
	public class Body {

		private GameObject _gameObject;

		private float _width;
		private float _height;
		private float _depth;
		private float _widthH;
		private float _heightH;
		private float _depthH;
        private float _widthQ;
        private float _heightQ;
        private float _depthQ;
        private RectTransform _rect;

		public BoxCollider2D collider;
		public SpriteRenderer renderer;
		
		public bool _isEmpty = false;

		private Body _parent;

		//private GeoBehaviour _behaviour;

		//private Dictionary<string, byte> _tags = new Dictionary<string, byte>();
		
		public Body(GameObject gameObject, bool isEmpty = false) {

			if(gameObject != null) {
				this._isEmpty = isEmpty;
				this.gameObject = gameObject;
			}
		}

		/*
		public GeoBehaviour behaviour {
			
			get{ return this._behaviour; }
			set{
				this._behaviour = value;
				this._behaviour.body = this;
			}
		}
		*/

		private void _ResetSize() {
			
			//Vector3 size = this.renderer == null ? (this.collider == null ? Vector3.zero : this.collider.bounds.size) : this.renderer.bounds.size;
			Vector3 size = (Vector3)this._rect.sizeDelta;
            
			if(size != Vector3.zero) {
				this._width  = size.x;
				this._height = size.y;
				this._depth  = size.z;
				this._widthH  = this._width/2;
				this._heightH = this._height/2;
				this._depthH  = this._depth/2;
                this._widthQ = this._width / 4;
                this._heightQ = this._height / 4;
                this._depthQ = this._depth / 4;
            }
		}

		public float width {

			get{ return this._width; }
		}

		public float height {

			get{ return this._height; }
		}

		public float depth {

			get{ return this._depth; }
		}

		public float widthH {

			get{ return this._widthH; }
		}

		public float heightH {

			get{ return this._heightH; }
		}

		public float depthH {

			get{ return this._depthH; }
		}

        public float widthQ {

			get{ return this._widthQ; }
		}

		public float heightQ {

			get{ return this._heightQ; }
		}

		public float depthQ {

			get{ return this._depthQ; }
		}

		public float x {

			get{ return this.position.x; }
			set{
            	this.position = new Vector3(value, this.position.y, this.position.z);
			}
		}

		public float y {

			get{ return this.position.y; }
			set{
				this.position = new Vector3(this.position.x, value, this.position.z);
			}
		}

		public float z {

			get{ return this.position.z; }
			set{
				this.position = new Vector3(this.position.x, this.position.y, value);
			}
		}

		public float xAngle {

			get{ return this.transform.eulerAngles.x; }
			set{
				this.angles = new Vector3(value, this.yAngle, this.zAngle);
			}
		}

		public float yAngle {

			get{ return this.transform.eulerAngles.y; }
			set{
				this.angles = new Vector3(this.xAngle, value, this.zAngle);
			}
		}

		public float zAngle {

			get{ return this.transform.eulerAngles.z; }
			set{
				this.angles = new Vector3(this.xAngle, this.yAngle, value);
			}
		}

		public float top {

			get{ return this.y + this._heightH; }
			set{
				this.y = value - this._heightH;
			}
		}

		public float bottom {

			get{ return this.y - this._heightH; }
			set{
				this.y = value + this._heightH;
			}
		}

		public float left {

			get{ return this.x - this._widthH; }
			set{
				this.x = value + this._widthH;
			}
		}

		public float right {

			get{ return this.x + this._widthH; }
			set{
				this.x = value - this._widthH;
			}
		}

		public float front {

			get{ return this.z - this._depthH; }
			set{
				this.z = value + this._depthH;
			}
		}

		public float back {

			get{ return this.z + this._depthH; }
			set{
				this.z = value - this._depthH;
			}
		}

		public Vector3 position {

			get{ return this._gameObject.transform.position; }
			set{

				this.gameObject.transform.position = value;
			}
		}

		public Vector3 angles {

			get{ return this._gameObject.transform.eulerAngles; }
			set{

				this.gameObject.transform.eulerAngles = value;
			}
		}

		public Vector3 positionOnScreen {

			get{ return UnityEngine.Camera.main.WorldToScreenPoint(this.position); }
		}

		public Vector3 size {

			get{ return new Vector3(this._width, this._height, this._depth); }
			set {

                //if (this.name == "Parallax_01") Debug.Log(value);
                this._rect.sizeDelta = new Vector2(value.x, value.y);
				this._ResetSize();
			}
		}

        public Vector2 pivot {

            get { return this._rect.pivot; }
            set {

               this._rect.pivot = value;
            }
        }

        public string name {

			get{ return this.gameObject.name; }
		}

		public string tag {

			get{ return this.gameObject.tag; }
		}

		public GameObject gameObject {

			get{ return this._gameObject; }
			set{ 
				this._gameObject = value;
				
				if(this._isEmpty == false) {
				
					this.collider = this._gameObject.GetComponent<BoxCollider2D>();
					this.renderer = this._gameObject.GetComponent<SpriteRenderer>();
                    this._rect = this._gameObject.GetComponent<RectTransform>();
                    if(this._rect == null) this._rect = this._gameObject.AddComponent<RectTransform>();
					
					if(this.collider != null) this.size = (Vector3)this.collider.bounds.size;
					else if(this.renderer != null) this.size = (Vector3)this.renderer.bounds.size;
					else { this.size = Vector3.zero; }
				}
			}
		}

		public Body parent {
			get {

                if(this._parent == null) {

					Transform parentTransform = this.transform.parent;
					if(parentTransform != null) this._parent = new Body(parentTransform.gameObject);
				}

				return this._parent;
			}

            set {

                this._parent = value;
                this.transform.SetParent(value.transform);
            }
		}

		public Transform transform {

			get{ return this._gameObject.transform; }
		}

		/*
		public void AddTag(string tagName) {

			if(!this._tags.ContainsKey(tagName)) this._tags.Add(tagName, 1);
		}

		public void AddTags(string[] tags) {

			foreach(string tag in tags) this.AddTag(tag);
		}

		public bool HasTag(string tagName) {

			return this._tags.ContainsKey(tagName);
		}
		*/
	}
}