﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraFollow : MonoBehaviour {

	public CameraProperty property;
	// public Transform child;

	public float speed = .2f;
	public Transform target;

	private Transform _transform;

	Vector3 velocity;
	bool shaking = false;

	public static CameraFollow instance;

	void Awake(){
		instance = this;
		_transform = transform;
	}

	void Start () {
		
	}
	
	void FixedUpdate () {
		if(!target)
		return;

		Vector3 targetPos = new Vector3(target.position.x, target.position.y + property.height, property.zDistance);
		targetPos = new Vector3(Mathf.Clamp(targetPos.x, property.bound.left, property.bound.right), Mathf.Clamp(targetPos.y, property.bound.bottom, property.bound.top), targetPos.z);
		
			_transform.position = Vector3.SmoothDamp(_transform.position, targetPos, ref velocity, speed);

	}
}

[System.Serializable]
public struct CameraProperty{
	public float zDistance;
	public float height;
	public Bound bound;
}

[System.Serializable]
public struct Bound{
	public float left;
	public float right;
	public float top;
	public float bottom;
}