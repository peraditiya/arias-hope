﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public abstract class Character : MonoBehaviour {

        [HideInInspector]
        public Unit unit;

        [HideInInspector]
        public Animator animator;
        
        [HideInInspector]
        public Body body;

        [HideInInspector]
        public Vector3 movement = Vector3.zero;

        [HideInInspector]
        public int verticalIndex = 0;

        [HideInInspector]
        public float gravityScale;
        
        public static string EMPTY_ID = "Empty";
        public static string IDLE_ID = "Idle";
        public static string WALK_ID = "Forward";
        public static string JUMP_UP_ID = "JumpUp";
        public static string JUMP_DOWN_ID = "JumpDown";
        public static string GRAPPLING_FORWARD_ID = "GrapplingForward";
        public static string GRAPPLING_UP_FORWARD_ID = "GrapplingUpForward";
        public static string GRAPPLING_UP_ID = "GrapplingUp";
        public static string JUMP_GRAPPLING_FORWARD_ID = "JumpGrapplingForward";
        public static string JUMP_GRAPPLING_UP_FORWARD_ID = "JumpGrapplingUpForward";
        public static string JUMP_GRAPPLING_UP_ID = "JumpGrapplingUp";
        public static string ATTACK_01_ID = "Melee_01";
        public static string ATTACK_02_ID = "Melee_02";
        public static string ATTACK_03_ID = "Melee_03";
        public static string JUMP_ATTACK = "JumpAttack";
        public static string WITHDRAW = "Withdraw";

        public Vector3 speed = new Vector3(1, 1, 4);

        public virtual void Awake() {

            this.unit = new Unit(this);
            this.body = new Body(this.transform.Find("Body").gameObject);
            this.unit.pivot = new Vector2(0.5f, 0.5f);
            this.body.pivot = Vector2.zero;

            this.animator = this.GetComponentInChildren<Animator>();

            this.gravityScale = this.unit.rigidBody.gravityScale;
        }

        public virtual void Start() { }

        public virtual void Update() { }

        public virtual void FixedUpdate() {}

        protected virtual void Walk() {

            this.unit.movement = Vector3.Scale(this.movement, this.speed);
        }

        protected virtual void Idle() {

            this.unit.movement = Vector3.zero;
            //this.animator.SetInteger("State", CharacterAnimation.IDLE_ID);
        }

        protected virtual void Attack() {

            //this.animator.SetInteger("State", CharacterAnimation.ATTACK_01_ID);
        }

        protected virtual void Jump() {

            //this.animator.SetInteger("State", CharacterAnimation.ATTACK_01_ID);
        }

        protected ColliderInfo ScanCollider(Vector3 direction, float distance = 0.1f) {

            ColliderInfo ret = new ColliderInfo();

            Vector3[] rayPosition = new Vector3[3];

            if(direction == Vector3.left) {

                rayPosition[0] = new Vector3(this.body.left - 0.1f, this.body.bottom + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left - 0.1f, this.body.bottom + this.body.heightH, this.body.z);
                rayPosition[2] = new Vector3(this.body.left - 0.1f, this.body.top - 0.1f, this.body.z);

            } else if (direction == Vector3.right) {

                rayPosition[0] = new Vector3(this.body.right + 0.1f, this.body.bottom + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.right + 0.1f, this.body.bottom + this.body.heightH, this.body.z);
                rayPosition[2] = new Vector3(this.body.right + 0.1f, this.body.top - 0.1f, this.body.z);

            } else if (direction == Vector3.up) {

                rayPosition[0] = new Vector3(this.body.left + 0.1f, this.body.top + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left + this.body.widthH, this.body.top + 0.1f, this.body.z);
                rayPosition[2] = new Vector3(this.body.right - 0.1f, this.body.top + 0.1f, this.body.z);

            } else if (direction == Vector3.down) {

                rayPosition[0] = new Vector3(this.body.left + 0.1f, this.body.bottom - 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left + this.body.widthH, this.body.bottom - 0.1f, this.body.z);
                rayPosition[2] = new Vector3(this.body.right - 0.1f, this.body.bottom - 0.1f, this.body.z);
            }

            for(int i = 0; i < rayPosition.Length; i++) ret.AddCollider(Physics2D.Raycast(rayPosition[i], direction, distance));

            return ret;
        }
    }
}
