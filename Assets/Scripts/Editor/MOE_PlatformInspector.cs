﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SKY.Platform))]
public class MOE_PlatformInspector : Editor {

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SKY.Platform platform = (SKY.Platform)target;

        if (GUILayout.Button("Snap")) platform.Snap();
    }
}
