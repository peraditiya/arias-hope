﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SKY.Room))]
public class MOE_RoomInspector : Editor {

    private static SKY.Viewport _viewport;
    private Transform _holds;
    private Transform _meshes;
    private Transform _gates;
    private Transform _platform;
    private Transform _aiPoint;

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SKY.Room room = (SKY.Room)target;
        room.Awake();
        
        if (GUILayout.Button("Apply")) {

            GameObject grid;
            SpriteRenderer gridRenderer;

            if (room.body.transform.Find("Grid") == null) {

                grid = new GameObject("Grid");
                grid.transform.parent = room.body.transform;
                grid.transform.localPosition = new Vector3(0, 0, 0);
                gridRenderer = grid.AddComponent<SpriteRenderer>();

            } else {

                grid = room.body.transform.Find("Grid").gameObject;
                gridRenderer = grid.GetComponent<SpriteRenderer>();
            }

            gridRenderer.sprite = Resources.Load<Sprite>("Textures/Grid");
            gridRenderer.drawMode = SpriteDrawMode.Tiled;
            gridRenderer.size = new Vector2(room.body.width, room.body.height);

            if (room.use) {

                if(MOE_RoomInspector._viewport == null) MOE_RoomInspector._viewport = Camera.main.gameObject.GetComponent<SKY.Viewport>();
                MOE_RoomInspector._viewport.Awake();
                MOE_RoomInspector._viewport.Start();
                
                room.Start();
                MOE_RoomInspector._viewport.Update();
            }
        }

        if (GUILayout.Button("Add Hold")) {

            if (this._holds == null) {

                this._holds = room.gameObject.transform.Find("Holds");
                if(this._holds == null) {

                    GameObject holds = new GameObject("Holds");
                    holds.transform.parent = room.transform;
                    holds.transform.localPosition = new Vector3(0, 0, 0);
                    this._holds = holds.transform;
                }
            }

            GameObject hold = new GameObject("Hold_" + this._holds.childCount);
            hold.AddComponent<SKY.Hold>();
            hold.transform.parent = this._holds;
            hold.transform.localPosition = Vector3.zero;
            hold.tag = "Hold";
            hold.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Textures/Hold") as Sprite;
            hold.GetComponent<BoxCollider2D>().isTrigger = true;
        }

        if (GUILayout.Button("Add Mesh")) {

            if (this._meshes == null) {

                this._meshes = room.gameObject.transform.Find("Meshes");
                if (this._meshes == null) {

                    GameObject meshes = new GameObject("Meshes");
                    meshes.transform.parent = room.transform;
                    meshes.transform.localPosition = new Vector3(0, 0, 0);
                    this._meshes = meshes.transform;
                }
            }

            GameObject mesh = new GameObject("Mesh_" + this._meshes.childCount);
            mesh.AddComponent<SKY.AutoMesh>();
            mesh.transform.parent = this._meshes;
            mesh.transform.localPosition = Vector3.zero;
            mesh.tag = "Wall";
        }
        
        if (GUILayout.Button("Add Platform")) {

            if (this._platform == null) {

                this._platform = room.gameObject.transform.Find("Platform");
                if (this._platform == null) {

                    GameObject platforms = new GameObject("Platform");
                    platforms.transform.parent = room.transform;
                    platforms.transform.localPosition = new Vector3(0, 0, 0);
                    this._platform = platforms.transform;
                }
            }

            GameObject platform = new GameObject("Platform_" + this._platform.childCount);
            platform.AddComponent<SKY.Platform>();
            platform.transform.parent = this._platform;
            platform.transform.localPosition = Vector3.zero;
            platform.GetComponent<PolygonCollider2D>().points = new Vector2[] { new Vector2(-1, 0.3f), new Vector2(-1, 0), new Vector2(1, 0), new Vector2(1, 0.3f) };
            platform.GetComponent<PolygonCollider2D>().isTrigger = true;
        }

        if (GUILayout.Button("Add Ai Point")) {

            if (this._aiPoint == null) {

                this._aiPoint = room.gameObject.transform.Find("AiPoints");
                if (this._aiPoint == null) {

                    GameObject aiPoints = new GameObject("AiPoints");
                    aiPoints.transform.parent = room.transform;
                    aiPoints.transform.localPosition = new Vector3(0, 0, 0);
                    this._aiPoint = aiPoints.transform;
                }
            }

            GameObject aiPoint = new GameObject("AiPoint_" + this._aiPoint.childCount);
            aiPoint.AddComponent<SKY.AiPoint>();
            aiPoint.transform.parent = this._aiPoint;
            aiPoint.transform.localPosition = Vector3.zero;
            aiPoint.GetComponent<BoxCollider2D>().size = new Vector2(0.3f, 0.3f);
            aiPoint.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }
}