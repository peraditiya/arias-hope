﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SKY.Stage))]
public class MOE_StageInspector : Editor {

    private Transform _parallax;
    private Transform _room;
    private Transform _gates;

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SKY.Stage stage = (SKY.Stage)target;

        //this._parallax  = GameObject.Find("Parallax").transform;

        if (GameObject.Find("Parallax") == null) {

            this._parallax = new GameObject("Parallax").transform;
            this._parallax.parent = stage.transform;
            this._parallax.position = new Vector3(0, 0, 10);

        } else
            this._parallax = GameObject.Find("Parallax").transform;

        if (GameObject.Find("Room") == null) {

            this._room = new GameObject("Room").transform;
            this._room.parent = stage.transform;
            this._room.position = new Vector3(0, 0, 0);

        } else
            this._room = GameObject.Find("Room").transform;

        if (GameObject.Find("Gates") == null) {

            this._gates = new GameObject("Gates").transform;
            this._gates.parent = stage.transform;
            this._gates.position = new Vector3(0, 0, 0);

        } else
            this._gates = GameObject.Find("Gates").transform;

        if (GUILayout.Button("Add Room")) {

            GameObject room = new GameObject("New Room");
            room.AddComponent<SKY.Room>();
            room.gameObject.transform.parent = this._room;

            GameObject holds;
            GameObject layout;
            GameObject meshes;
            GameObject platform;
            GameObject aiPoint;

            holds = new GameObject("Holds");
            holds.transform.parent = room.transform;
            holds.transform.position = new Vector3(0, 0, 0);

            layout = new GameObject("Layout");
            layout.transform.parent = room.transform;
            layout.transform.position = new Vector3(0, 0, 5);

            platform = new GameObject("Platform");
            platform.transform.parent = room.transform;
            platform.transform.position = new Vector3(0, 0, 5);

            meshes = new GameObject("Meshes");
            meshes.transform.parent = room.transform;
            meshes.transform.position = new Vector3(0, 0, 0);

            aiPoint = new GameObject("AiPoints");
            aiPoint.transform.parent = room.transform;
            aiPoint.transform.position = new Vector3(0, 0, 0);
        }

        if (GUILayout.Button("Add Gate")) {

            GameObject gate = new GameObject("Gate_" + this._gates.childCount);
            gate.AddComponent<SKY.Gate>();
            gate.transform.parent = this._gates;
            gate.transform.localPosition = Vector3.zero;
            gate.GetComponent<BoxCollider2D>().size = new Vector2(0.4f, 2f);
            gate.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }
}
