﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class HostileCrawler : Hostile {

        private bool _isUpperFloor = false;

        // Use this for initialization
        public override void Awake() {

            base.Awake();

            this.unit.rigidBody.gravityScale = 0f;
            RaycastHit2D upCollider = this.ScanCollider(Vector3.up, 3f).getFirstAvailableCollider();
            RaycastHit2D downCollider = this.ScanCollider(Vector3.down, 3f).getFirstAvailableCollider();

            if ((upCollider.collider == null || upCollider.distance > downCollider.distance)) {

                this.unit.bottom = downCollider.point.y;
                this._isUpperFloor = false;
                this.unit.rigidBody.gravityScale = 2f;

            } else {

                this.unit.top = upCollider.point.y;
                this._isUpperFloor = false;
                this.unit.rigidBody.gravityScale = -2f;
            }
        }

        public override void Start() {

            this.aiRefreshDelay.phaseGap = 2f;
        }

        public override void Update() {

            if (this.aiRefreshDelay.IsPhasePassed() || this.unit.state.isOnTask == false) {

                if(this.trigger == null) {

                    int prob = Util.RandomPercent();

                    this.target = new Vector3(Root.player.unit.x, this.unit.y, this.unit.z);
                }

                this.unit.state.isOnTask = true;
            }

            base.Update();
        }

        public override void FixedUpdate() {

            if(this.unit.state.isJump == false) {

                if (this.movement.x < 0) {

                    Collider2D collider = this.ScanCollider(Vector3.left, 0.05f).getFirstAvailableCollider().collider;

                    if (collider != null) { this.unit.rigidBody.AddForce(new Vector2(0, 30f)); }

                } else if (this.movement.x > 0) {

                    Collider2D collider = this.ScanCollider(Vector3.right, 0.05f).getFirstAvailableCollider().collider;

                    if (collider != null) { this.unit.rigidBody.AddForce(new Vector2(0, 30f)); }
                }
            }

            base.FixedUpdate();
        }

        private void _SwitchGround() {

            if(!this.unit.state.isFalling && !this.unit.state.isJump) {

                RaycastHit2D upCollider = this.ScanCollider(Vector3.up, 3f).getFirstAvailableCollider();
                RaycastHit2D downCollider = this.ScanCollider(Vector3.down, 3f).getFirstAvailableCollider();

                this._isUpperFloor = upCollider.collider != null && upCollider.distance < 0.1f;

                if (this._isUpperFloor && downCollider.collider != null) this.unit.state.isFalling = true;
                else if (!this._isUpperFloor && upCollider.collider != null) this.unit.state.isJump = true;
                else { this.target = new Vector3((float)Random.Range(-2, 2), this.unit.y, this.unit.z); }
            }
        }

        private void _Jump() {

        }
    }
}