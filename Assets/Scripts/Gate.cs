﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    [RequireComponent(typeof(BoxCollider2D))]

    public class Gate : MonoBehaviour {

        [HideInInspector]
        public Body body;

        [HideInInspector]
        public Room parent;

        public Room link1;
        public Room link2;

        public void Start() {

            this.body = new Body(this.gameObject);

            this.gameObject.tag = "Gate";

            if (!Stage.gates.ContainsKey(this.gameObject.name)) Stage.gates.Add(this.gameObject.name, this);
		}
    }
}