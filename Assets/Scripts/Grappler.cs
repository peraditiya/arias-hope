﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class Grappler : MonoBehaviour {

        public Body body;

        public float height = 0.16f;

        public float throwSpeed = 0.31f;
        public float throwDistance = 6f;

        public float retractSpeed = 0.103f;
        public float retractSpeedDiagonal = 0.073f;

        public float swingSpeed = 1.5f;

        [HideInInspector]
        public Vector3 hookedPosition;

        void Start() {

            this.body = new Body(this.gameObject);
            this.body.pivot = new Vector2(0, 1);
            this.body.collider.size = this.body.size;
            this.body.collider.offset = new Vector2((0 - (this.height/2)), (0 - (this.height / 2)));
            this.size = new Vector2(this.height, this.height);

            Physics2D.IgnoreCollision(Root.player.unit.collider, this.body.collider, true);

            Root.player.grappler = this;
        }

        void OnTriggerEnter2D(Collider2D collider) {

            if(collider.tag == "Hold" && (Root.player.currentHold == null || Root.player.currentHold.name != collider.name)) {

                Root.player.currentHold = Stage.currentRoom.GetHold(collider.name);

                Vector2 grapplingDirection = Root.player.grapplingDirection;

                if ((grapplingDirection.x == 1 || grapplingDirection.x == -1) && grapplingDirection.y == 1) {

                    float adjacentSide = Mathf.Sin(grapplingDirection.x * 45) * (this.body.width * 0.8f);

                    this.hookedPosition.x = Root.player.unit.x + adjacentSide;
                    this.hookedPosition.y = Root.player.unit.y + adjacentSide;
                    
                } else
                    this.hookedPosition = collider.transform.position;

                this.body.position = this.hookedPosition;
                
                if (grapplingDirection.x == 1 && grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(180, 180, this.transform.eulerAngles.z);
                else if (grapplingDirection.x == -1 && grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(180, 0, this.transform.eulerAngles.z);
                else if (grapplingDirection.x == 1 && grapplingDirection.y == 0) this.transform.eulerAngles = new Vector3(180, 0, 180);
                else if (grapplingDirection.x == -1 && grapplingDirection.y == 0) this.transform.eulerAngles = new Vector3(180, 0, 0);
                else if (grapplingDirection.x == 0 && grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(180, 0, 90);
            }
        }

        public Vector2 size {

            get {
                return this.body.size;
            }

            set {

                this.body.size = value;
                this.body.renderer.size = this.body.size;
                //this.body.collider.size = this.body.size;
            }
        }

        public void Enable() {

            this.body.position = Root.player.unit.position;
            this.enabled = true;
        }

        public void Disable() {

            this.body.position = new Vector3(1000, 1000, 0);
            this.size = new Vector2(0.16f, 0.16f);
            this.enabled = false;
        }
    }
}