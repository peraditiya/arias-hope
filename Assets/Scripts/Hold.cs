﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]

    public class Hold : MonoBehaviour {

        [HideInInspector]
        public Body body;

        [HideInInspector]
        public Room parent;

        // Use this for initialization
        public void Start() {

            this.body = new Body(this.gameObject);

            this.gameObject.tag = "Hold";

            this.parent = this.gameObject.transform.parent.parent.GetComponent<Room>();
            this.parent.AddHold(this);
        }
    }
}