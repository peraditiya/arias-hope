﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class Hostile : Character {

        protected TimeUtil aiRefreshDelay;
        protected Body trigger;

        private Vector3 _target;

        // Use this for initialization
        public override void Awake() {

            base.Awake();

            this.aiRefreshDelay = new TimeUtil();
            this.target = Vector3.zero;
        }

        public override void Start() {

        }

        public override void Update() {

            this.movement = Vector3.zero;

            if (this.target.x != (float)Math.Round(this.unit.x, 1)) this.movement.x = (this.unit.x > this.target.x) ? -0.02f : 0.02f;
            if (this.target.y != (float)Math.Round(this.unit.y, 1)) this.movement.y = (this.unit.y > this.target.y) ? -0.02f : 0.02f;
            if (this.target.z != (float)Math.Round(this.unit.z, 1)) this.movement.z = (this.unit.z > this.target.z) ? -0.02f : 0.02f;

            if (this.movement == Vector3.zero) this.unit.state.isOnTask = false;
        }

        public override void FixedUpdate() {

            this.unit.movement = this.movement;
        }

        protected Vector3 target {

            get {
                return this._target;
            }

            set {
                this._target = new Vector3((float)Math.Round(value.x, 1), (float)Math.Round(value.y, 1), (float)Math.Round(value.z, 1));
            }
        }
    }
}
