﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class MovingObject : Body {

		public float speed;

		public MovingObject(GameObject obj, float speed = 0.1f) : base(obj) {

			this.speed = speed;
		}
	}
}