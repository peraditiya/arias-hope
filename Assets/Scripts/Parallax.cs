﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Parallax : Background {

        // Use this for initialization

        public int width = 0;
        public int height = 0;

        public Vector2 speed = new Vector2(1, 1);
        
        void Start () {

			this.body = new Body(this.gameObject);

            Vector2 size = Vector2.zero;
            size.x = (this.width > 0 && this.width < 100) ? ((float)this.width/100) * Stage.currentRoom.body.width : this.body.width;
            size.y = (this.height > 0 && this.width < 100) ? ((float)this.height / 100) * Stage.currentRoom.body.height : this.body.height;
            if (size != Vector2.zero) this.body.size = size;
            
            Root.viewport.parallax.Add(this);
		}
	}
}