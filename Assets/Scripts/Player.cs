﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]

    public class Player : Character
    {

        private int combo = 0;
        private UnitState buttonPressed;
        private Vector3 newMovement;

        private bool _jumpExec = false;
        private bool _grappleExec = false;
        private float swingLength = 6f;

        [HideInInspector]
        public Vector2 grapplingDirection = Vector2.zero;
        [HideInInspector]
        public Grappler grappler;
        //public Grappler grappler;        

        [HideInInspector]
        public SpriteAnimator spriteAnimator;

        private Gate currentGate;
        private Collider2D currentPlatform;
        [HideInInspector]
        public Hold currentHold;

        private IEnumerator jumpRoutine;
        private IEnumerator grapplingRoutine;
        private IEnumerator attackRoutine;
        private IEnumerator retractRoutine;
        private IEnumerator swingRoutine;

        public void Awake()
        {

            base.Awake();

            Root.player = this;

            this.buttonPressed = new UnitState();
        }

        public override void Start()
        {

            base.Start();

            this.spriteAnimator = new SpriteAnimator(this.body, "Textures/Shanoa");

            SpriteAnimation tAnim = this.spriteAnimator.CreateAnimation(Character.IDLE_ID);
            tAnim.AddFrame(0).AddFrame(1).AddFrame(2).AddFrame(3).AddFrame(4).AddFrame(5).AddFrame(6).AddFrame(7).AddFrame(8).AddFrame(9).loop = true;

            tAnim = this.spriteAnimator.CreateAnimation(Character.WALK_ID);
            tAnim.AddFrame(10).AddFrame(11).AddFrame(13).AddFrame(16).AddFrame(18).AddFrame(20).AddFrame(22).AddFrame(26).AddFrame(29).AddFrame(31).AddFrame(32).loop = true;
            tAnim.delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_UP_ID);
            tAnim.AddFrame(33).AddFrame(34).AddFrame(35).AddFrame(36).AddFrame(37).AddFrame(38).AddFrame(39).AddFrame(40).AddFrame(41).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_DOWN_ID);
            tAnim.AddFrame(42).AddFrame(43).AddFrame(44).AddFrame(45).AddFrame(46).AddFrame(47).AddFrame(48).AddFrame(49).AddFrame(50).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_FORWARD_ID);
            tAnim.AddFrame(53).AddFrame(54).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_UP_FORWARD_ID);
            tAnim.AddFrame(53).AddFrame(57).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_UP_ID);
            tAnim.AddFrame(53).AddFrame(58).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.ATTACK_01_ID);
            tAnim.AddFrame(59).AddFrame(60).AddFrame(61).AddFrame(62).AddFrame(64).AddFrame(65).AddFrame(66).AddFrame(68).AddFrame(70).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_ATTACK);
            tAnim.AddFrame(71).AddFrame(72).AddFrame(73).AddFrame(74).AddFrame(75).AddFrame(76).AddFrame(78).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_FORWARD_ID);
            tAnim.AddFrame(78).AddFrame(79).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_UP_FORWARD_ID);
            tAnim.AddFrame(78).AddFrame(82).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_UP_ID);
            tAnim.AddFrame(78).AddFrame(80).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.WITHDRAW);
            tAnim.AddFrame(71).delay = 0.05f;

            //this._floatingInterval.phaseGap = 0.05f;

            this.unit.state.isJump = true;
            this.unit.state.isFalling = true;
            this.spriteAnimator.currentAnimation = Character.IDLE_ID;
        }

        // Update is called once per frame
        public override void Update()
        {

            this.newMovement = Vector3.zero;

            if (Input.GetKey(KeyCode.D)) this.newMovement.x = 1;
            else if (Input.GetKey(KeyCode.A)) this.newMovement.x = -1;

            if (Input.GetKey(KeyCode.W)) this.newMovement.y = 1f;
            else if (Input.GetKey(KeyCode.S)) this.newMovement.y = -1f;

            if (Input.GetKey(KeyCode.G) && this._jumpExec == false) this._jumpExec = this.buttonPressed.isJump = true;
            else if (Input.GetKeyUp(KeyCode.G)) this._jumpExec = this.buttonPressed.isJump = false;

            if (Input.GetKey(KeyCode.V) && this._grappleExec == false) this._grappleExec = this._grappleExec = this.buttonPressed.isGrappling = true;
            else if (Input.GetKeyUp(KeyCode.V)) this._grappleExec = this.buttonPressed.isGrappling = false;

            if (Input.GetKey(KeyCode.F)) this.buttonPressed.isAttacking = true;

            if (this.newMovement.x != 0) this.body.renderer.flipX = this.newMovement.x < 0;

            if (this.unit.state.isHanging == true) this.Hanging();
            else if (this.unit.state.isSwing == true) this.Swing();
            else if (this.unit.state.isRetract == true) this.Retract();
            else if (this.buttonPressed.isGrappling == true) this.Grappling();
            else if (this.buttonPressed.isJump == true) this.Jump();
            else if (this.buttonPressed.isFalling == true) this.Jump();
            else if (this.buttonPressed.isAttacking == true) this.Attack();
            else if (this.unit.state.isIdle == true)
            {

                if (this.movement.x != 0) this.spriteAnimator.currentAnimation = Character.WALK_ID;
                else { this.spriteAnimator.currentAnimation = Character.IDLE_ID; }
            }

            if (this.unit.state.isFreeze == true) this.newMovement.x = 0;

            this.movement.x = this.newMovement.x;

            this.buttonPressed.Reset();
            this.spriteAnimator.Play();
        }

        public override void FixedUpdate()
        {

            this.unit.movement = Vector3.Scale(this.movement, this.speed) * Time.deltaTime;
            //if (this.unit.state.isFreeze != true) this.unit.rigidBody.velocity = new Vector2(this.movement.x * this.speed.x * Time.deltaTime, this.unit.rigidBody.velocity.y);
        }

        protected override void Attack()
        {

            //this._RegisterIdle();
        }

        protected override void Jump()
        {

            this.unit.rigidBody.gravityScale = this.gravityScale;

            if (this.unit.state.isJump == false)
            {

                if (this.unit.state.isFalling == false && this.currentPlatform != null
                    && this.buttonPressed.isJump == true && this.newMovement.y < 0)
                {

                    this.currentPlatform.isTrigger = true;
                    this.unit.state.isJump = true;
                    this.unit.state.isFalling = true;
                    this.verticalIndex = 0;

                }
                else
                {

                    this.unit.state.isJump = true;
                    this.verticalIndex = (int)this.speed.z;
                }

                this.jumpRoutine = this.JumpRoutine();
                this.StartCoroutine(this.jumpRoutine);

            }
            else if (this.unit.state.isFalling == true)
            {

                this.spriteAnimator.currentAnimation = Character.JUMP_DOWN_ID;
            }
        }

        private IEnumerator JumpRoutine()
        {

            while (this.unit.state.isJump == true)
            {

                string animationId = Character.JUMP_UP_ID;

                if (this.verticalIndex <= 0)
                {

                    this.unit.state.isFalling = true;

                    animationId = Character.JUMP_DOWN_ID;

                }
                else
                {

                    if (this._jumpExec == false) this.verticalIndex = 0;
                    else { this.unit.rigidBody.AddForce(1.4f * Vector2.up * this.unit.rigidBody.mass, ForceMode2D.Impulse); }
                }

                this.spriteAnimator.currentAnimation = animationId;

                this.verticalIndex--;

                yield return new WaitForFixedUpdate();
            }
        }

        private void JumpStop()
        {

            this.unit.state.isJump = false;
            this.unit.state.isFalling = false;
            this.spriteAnimator.currentAnimation = Character.IDLE_ID;
        }

        private void Grappling()
        {

            if (this.unit.state.isGrappling == false)
            {

                this.unit.state.isGrappling = true;
                this.unit.state.isFreeze = true;

                string grapplingState = Character.GRAPPLING_FORWARD_ID;
                Vector3 grapplingDeg = Vector3.zero;

                this.grapplingDirection = this.newMovement != Vector3.zero ? (Vector2)this.newMovement : (this.body.renderer.flipX ? Vector2.left : Vector2.right);

                if (this.grapplingDirection.x == 1 && this.grapplingDirection.y == 1)
                {

                    grapplingState = Character.GRAPPLING_UP_FORWARD_ID;
                    grapplingDeg.z = 45;

                }
                else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 1)
                {

                    grapplingState = Character.GRAPPLING_UP_FORWARD_ID;
                    grapplingDeg = new Vector3(0, 180, 45);

                }
                else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 0)
                {

                    grapplingState = Character.GRAPPLING_FORWARD_ID;
                    grapplingDeg = new Vector3(0, 180, 0);

                }
                else if (this.grapplingDirection.x == 0 && this.grapplingDirection.y == 1)
                {

                    grapplingState = Character.GRAPPLING_UP_ID;
                    grapplingDeg = new Vector3(0, 0, 90);
                }

                this.grappler.Enable();
                this.grappler.transform.eulerAngles = grapplingDeg;

                this.spriteAnimator.currentAnimation = grapplingState;

                this.currentHold = null;

                this.grapplingRoutine = this.GrapplingRoutine();
                this.StartCoroutine(this.grapplingRoutine);
            }
        }

        IEnumerator GrapplingRoutine()
        {

            int scaler = 1;

            while (this.unit.state.isGrappling == true)
            {

                if (this.grappler.body.width > 0)
                {

                    if (this.currentHold == null)
                    {

                        if (this.unit.state.isJump == true) this.grappler.body.y = this.unit.y; 
                        if (scaler == 1) scaler = this.grappler.body.width >= this.grappler.throwDistance ? -1 : 1;
                        
                        this.grappler.size = new Vector3(this.grappler.body.width + (this.grappler.throwSpeed * scaler), this.grappler.height, 1f);
                        this.grappler.body.collider.offset = new Vector2((this.grappler.body.right - this.grappler.body.left) - (this.grappler.height / 2), this.grappler.body.collider.offset.y);
                        
                    }
                    else
                        this.Retract();

                }
                else
                    this.GrapplingStop();

                yield return new WaitForFixedUpdate();
            }
        }

        private void GrapplingStop()
        {

            this.unit.state.isGrappling = false;
            this.unit.state.isFreeze = false;
            this.grappler.Disable();
        }

        private void Retract(bool isFullRetract = false)
        {

            if (this.unit.state.isRetract == false)
            {

                this.unit.state.isGrappling = false;
                this.unit.state.isSwing = false;
                this.unit.state.isRetract = true;

                this.unit.rigidBody.gravityScale = 0;

                this.spriteAnimator.currentAnimation = Character.JUMP_UP_ID;

                this.swingLength = 0.7f * this.grappler.body.width;

                this.retractRoutine = this.RetractRoutine(isFullRetract);
                this.StartCoroutine(this.retractRoutine);
            }
        }

        IEnumerator RetractRoutine(bool isFullRetract = false)
        {

            while (this.unit.state.isRetract == true)
            {

                if ((this.grappler.body.width > this.swingLength) || (isFullRetract && this.grappler.body.width > 0))
                {

                    this.grappler.size = new Vector3(this.grappler.body.width - this.grappler.retractSpeed, this.grappler.height, 1f);
                    this.grappler.body.collider.offset = new Vector2((this.grappler.body.right - this.grappler.body.left) - (this.grappler.height / 2), this.grappler.body.collider.offset.y);

                    this.unit.position = (Vector3)this.grappler.body.collider.bounds.center;

                }
                else if (isFullRetract == false)
                {

                    this.Swing();

                }
                else
                    this.Hanging();

                yield return new WaitForFixedUpdate();
            }
        }

        private void Swing()
        {

            if (this.unit.state.isSwing == false)
            {

                this.unit.state.isSwing = true;
                this.unit.state.isRetract = false;
                this.StopCoroutine(this.retractRoutine);

                this.swingRoutine = this.SwingRoutine();
                this.StartCoroutine(this.swingRoutine);

            }
            else
            {
                if (this.buttonPressed.isJump)
                {

                    this.HangingStop();
                    this.Jump();

                }
                else if (this.buttonPressed.isGrappling)
                    this.Retract(true);
            }
        }

        IEnumerator SwingRoutine()
        {

            int scaler = 0;

            while (this.unit.state.isSwing == true)
            {

                Vector3 grapplerDeg = this.grappler.transform.eulerAngles;

                if (scaler != 0)
                {

                    if (grapplerDeg.z <= 225) scaler = 1;
                    else if (grapplerDeg.z >= 315) scaler = -1;

                }
                else
                    scaler = grapplerDeg.z < 270 ? 1 : -1;

                grapplerDeg.z += this.grappler.swingSpeed * scaler;
                this.grappler.transform.eulerAngles = grapplerDeg;

                this.unit.position = (Vector3)this.grappler.body.collider.bounds.center;

                yield return new WaitForFixedUpdate();
            }
        }

        private void Hanging()
        {

            if (this.unit.state.isHanging == false)
            {

                this.unit.state.isHanging = true;
                this.unit.state.isSwing = false;
                this.unit.state.isRetract = false;
                this.grappler.transform.eulerAngles = new Vector3(this.grappler.transform.eulerAngles.x, this.grappler.transform.eulerAngles.y, 270);

                this.StopCoroutine(this.swingRoutine);

            }
            else
            {

                if (this.buttonPressed.isJump == true)
                {
                    this.HangingStop();
                    this.Jump();
                }
                else if (this.newMovement.y < 0)
                {

                    if (this.grappler.body.width < this.swingLength || (this.swingLength == 0 && this.grappler.body.width < this.grappler.throwDistance))
                    {

                        this.unit.state.isSwing = true;

                        this.grappler.size = new Vector3(this.grappler.body.width + this.grappler.retractSpeed, this.grappler.height, 1f);
                        this.grappler.body.collider.offset = new Vector2((this.grappler.body.right - this.grappler.body.left) - (this.grappler.height / 2), this.grappler.body.collider.offset.y);

                        this.unit.position = (Vector3)this.grappler.body.collider.bounds.center;
                    }
                }
                else
                {

                    if (this.unit.state.isSwing == true)
                    {
                        if (this.newMovement.y > 0)
                        {

                            if (this.grappler.body.width > 0)
                            {

                                this.grappler.size = new Vector3(this.grappler.body.width - this.grappler.retractSpeed, this.grappler.height, 1f);
                                this.grappler.body.collider.offset = new Vector2((this.grappler.body.right - this.grappler.body.left) - (this.grappler.height / 2), this.grappler.body.collider.offset.y);

                                this.unit.position = (Vector3)this.grappler.body.collider.bounds.center;

                            }
                            else
                                this.unit.state.isSwing = false;

                        }
                        else if (this.newMovement.x != 0)
                        {

                            this.unit.state.isSwing = false;
                            this.unit.state.isHanging = false;

                            this.grappler.body.zAngle -= this.newMovement.x;

                            this.Swing();
                        }
                    }
                }
            }
        }

        private void HangingStop()
        {

            this.unit.state.isHanging = false;
            this.unit.state.isSwing = false;
            this.unit.state.isFreeze = false;
            this.grappler.Disable();

            if (this.newMovement.y < 0)
            {

                this.unit.state.isJump = true;
                this.unit.state.isFalling = true;
            }

        }

        private void OnCollisionStay2D(Collision2D collision)
        {

            if (collision.collider.tag == "Wall" || collision.collider.tag == "Platform")
            {

                if (this.unit.state.isFalling == true)
                {

                    RaycastHit2D hit = Util.HitScan(this.unit, Vector2.down);

                    if (hit.collider != null) this.JumpStop();
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {

            if (collision.collider.tag != "Platform" || (collision.collider.tag == "Platform" && (this.currentPlatform != null && this.currentPlatform.name != collision.collider.name)))
            {

                this.currentPlatform = null;
            }
            else if (collision.collider.tag != "Wall")
            {

                if (this.unit.state.isSwing) this.HangingStop();
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {

            if (collision.collider.tag == "Platform")
            {

                collision.collider.isTrigger = true;
                this.currentPlatform = null;
            }

            if (Util.HitScan(this.unit, Vector2.down).collider == null)
            {

                this.unit.state.isFalling = true;
                this.unit.state.isJump = true;
                this.Jump();
            }
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {

            if (collider.tag == "Platform")
            {

                if (this.unit.state.isFalling == true && Util.HitScan(this.unit, Vector2.down).collider != null)
                {

                    collider.isTrigger = false;
                    this.currentPlatform = collider;
                    this.unit.rigidBody.velocity = Vector2.zero;

                    this.JumpStop();
                }
            }
        }
    }
}
