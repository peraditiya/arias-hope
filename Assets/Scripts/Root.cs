﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Root {

		public static Player player;

        public static float cellWidthPx = 1920;
        public static float cellHeightPx = 1080;

        public static float cellWidth = Root.cellWidthPx / 100;
		public static float cellHeight = Root.cellHeightPx / 100;

		public static Body platform = new Body(GameObject.Find("Platform"), true);

		//public static Body background = new Body(GameObject.Find("Background"));

        public static Viewport viewport;
    }
}