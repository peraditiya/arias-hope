using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
      
      public class SpriteAnimation {

            private List <int> _frames;
            private int _frameLength = 0;

            public bool loop = false;
            public float delay = 0.1f;
            public int currentIndex = -1;

            private bool _isAnimationComplete = false;
            private int _index = -1;
            
            public SpriteAnimation() {

                  this._frames = new List <int> ();
            }

            public SpriteAnimation AddFrame(int frameId) {

                  this._frames.Add(frameId);

                  this._frameLength++;

                  return this;
            }

            public void Reset() {

                  this._index = -1;
                  this._isAnimationComplete = false;
                  this.currentIndex = -1;
            }

            public static SpriteAnimation operator + (SpriteAnimation animation, int frameId) {

                  return animation.AddFrame(frameId);
            }

            public int GetFrame(int frameIndex) {

                  return this._frames[frameIndex];
            }

            public int frame {

                  get {

                        if(this._index == -1) this._index = 0; 

                        this.currentIndex = this._index;

                        this._index += 1;

                        if (this._index >= this._frameLength) {

                              this._index = this.loop ? 0 : this.currentIndex;
                              this._isAnimationComplete = true;
                        } 
                        
                        return this._frames[this.currentIndex];
                  }
            }

            public int frameLength {

                  get {
                        return this._frameLength;
                  }
            }

            public bool isAnimationComplete {

                  get {
                        return this._isAnimationComplete;
                  }
            }    
      }
}