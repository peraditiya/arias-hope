﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class Stage : MonoBehaviour {

        public static Dictionary<string, Room> rooms = new Dictionary<string, Room>();

        public static Dictionary<string, Gate> gates = new Dictionary<string, Gate>();

        private static Room _currentRoom;

        void Awake() {

            if(Application.isPlaying == false) {

                if(this.transform.Find("Room") == null) {

                    GameObject room = new GameObject("Room");
                    room.transform.parent = this.transform;
                    room.transform.localPosition = Vector3.zero;
                }

                if (this.transform.Find("Gates") == null) {

                    GameObject gates = new GameObject("Gates");
                    gates.transform.parent = this.transform;
                    gates.transform.localPosition = Vector3.zero;
                }
            }
        }

        // Use this for initialization
        void Start() {}

        public static Room currentRoom {
            get {
                return Stage._currentRoom;
            }
            set {
                if (Stage._currentRoom != null) Stage._currentRoom.gameObject.SetActive(false);
                Stage._currentRoom = value;
                Stage._currentRoom.gameObject.SetActive(true);
            }
        }
    }
}