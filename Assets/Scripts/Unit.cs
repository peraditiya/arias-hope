﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Unit : Body {

		public Vector3 currentMovement;
		public MonoBehaviour monoBehaviour;
		public Rigidbody2D rigidBody;
		//public Body previousCollider;
		public UnitState state;

        private Character _character;
		
		// Use this for initialization
		public Unit (MonoBehaviour monoBehaviour) : base(monoBehaviour.gameObject) {
			
			this.monoBehaviour = monoBehaviour;

			this.state 	  = new UnitState();
			this.rigidBody  = this.gameObject.GetComponent<Rigidbody2D>() as Rigidbody2D;
		}

		public Unit (GameObject gameObject) : base(gameObject) {
			
			//this.state 	  = new UnitState();
			this.rigidBody  = this.gameObject.GetComponent<Rigidbody2D>() as Rigidbody2D;
		}

		public Vector3 movement {

			get { return this.currentMovement; }
			set {

				this.currentMovement = value;
					
				//this.rigidBody.velocity = this.currentMovement;
				this.rigidBody.transform.position += value;
			}
		}

        public Character character {

            get { return this._character; }
            set {

                this._character = value;
                this._character.unit = this;
            }
        }
    }
}