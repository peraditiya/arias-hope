﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class UnitState {

        public bool isJump;
        public bool isAttacking;
        public bool isGrappling;
        public bool isRetract;
        public bool isHanging;
        public bool isFacingBack;
        public bool isSwing;
        public bool isHit;
        public bool isHitEnemy;
        public bool isFalling;
        public bool isFreeze;

        //for enemy
        public bool isOnTask;

        public UnitState() { }

        public void Reset() {

            this.isJump = this.isAttacking = this.isGrappling = this.isRetract = this.isHanging = this.isFacingBack = this.isHit = this.isHitEnemy = false;
        }

        public bool isIdle {

            get {
                return !this.isAttacking && !this.isJump && !this.isGrappling && !this.isRetract && !this.isHanging && !this.isSwing && !this.isHit && !this.isHitEnemy && !this.isFalling;
            }
        }
    }
}