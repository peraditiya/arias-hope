﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class ColliderInfo {

        private RaycastHit2D[] _raycasts = new RaycastHit2D[3];
        private RaycastHit2D _firstAvailableCollider;
        private RaycastHit2D _lastAvailableCollider;
        private bool _isAnyCollision = false;
        private int _index = 0;

        public ColliderInfo() { }

        public bool IsAnyCollision() {

            return this._isAnyCollision;
        }

        public void AddCollider(RaycastHit2D raycast) {

            if (this._index < 3) {

                if (raycast.collider != null) {

                    this._isAnyCollision = true;

                    if (this._firstAvailableCollider.collider == null) this._firstAvailableCollider = raycast;
                    else if (this._firstAvailableCollider.collider != null) this._lastAvailableCollider = raycast;
                }

                this._raycasts[this._index] = raycast;

                this._index++;
            }
        }

        public RaycastHit2D getTopCollider() { return this._raycasts[2]; }
        public RaycastHit2D getRightCollider() { return this._raycasts[2]; }

        public RaycastHit2D getBottomCollider() { return this._raycasts[0]; }
        public RaycastHit2D getLeftCollider() { return this._raycasts[0]; }

        public RaycastHit2D getMiddleCollider() { return this._raycasts[1]; }

        public RaycastHit2D getFirstAvailableCollider() { return this._firstAvailableCollider; }
        public RaycastHit2D getLastAvailableCollider() { return this._lastAvailableCollider; }
    }
}