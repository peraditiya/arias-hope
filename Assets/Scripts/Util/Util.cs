using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Util {

		static public float DistanceToDegress(Vector3 basePosition, Vector3 targetPosition) {

			Vector3 diffPosition = basePosition - targetPosition;
			float phyta = Mathf.Sqrt(Mathf.Pow(diffPosition.x, 2f) + Mathf.Pow(diffPosition.y, 2f));
			float ret 	= Mathf.Rad2Deg*Mathf.Asin(diffPosition.y/phyta);

			if( (diffPosition.x < 0 && diffPosition.y < 0) || diffPosition.x < 0 ) ret = 180 - ret;
			else if(diffPosition.y < 0 ) ret += 360;

			return 360 - ret;
		}

		static public float DirectionToDegress(Vector3 direction) {

			float ret = 0;
				
			if(direction.z == 0) ret = direction.x < 0 ? 180 : 0; 				
			else if(direction.x == 0) ret = direction.z < 0 ? 90 : 270; 
			else if(direction.x == 1) ret = direction.z < 0 ? 45 : 315;
			else if(direction.x == -1) ret = direction.z < 0 ? 135 : 225;

			return ret;
		}

		static public Vector3 DegressToDirection(float degress) {

			Vector3 ret = Vector3.zero;
			//Debug.Log(degress);
			//Debug.Log("(" + degress + " == " + 225 + ") = " + (degress == 225));
			
			if(degress == 0) ret = new Vector3(1, 0, 0); 				
			else if(degress == 45) ret = new Vector3(1, 0, -1);
			else if(degress == 90) ret = new Vector3(0, 0, -1);
			else if(degress == 135) ret = new Vector3(-1, 0, -1);
			else if(degress == 180) ret = new Vector3(-1, 0, 0);
			else if(degress == 225) ret = new Vector3(-1, 0, 1);
			else if(degress == 270) ret = new Vector3(0, 0, 1);
			else if(degress == 315) ret = new Vector3(1, 0, 1);
			
			return ret;
		}

		static public Vector3 GetDistance(Vector3 self, Vector3 target) {

			return target - self;
		}

		static public int RandomPercent() {

			return Random.Range(1, 101);
		}

        static public RaycastHit2D HitScan(Body body, Vector2 direction, float distance = 0.1f) {

            RaycastHit2D ret = new RaycastHit2D();

            Vector2[] scanPosition = new Vector2[2];

            if(direction == Vector2.down) scanPosition = new Vector2[2] { new Vector2(body.left + 0.01f, body.bottom + 0.1f), new Vector2(body.right - 0.01f, body.bottom + 0.1f) };

            for (int x = 0; x < scanPosition.Length; x++) {

                RaycastHit2D[] scanHit = Physics2D.RaycastAll(scanPosition[x], Vector2.down, 0.1f + distance);

                if (scanHit.Length > 1) {

                    ret = scanHit[1];
                    break;
                }
            }

            return ret;
        }
    }
}