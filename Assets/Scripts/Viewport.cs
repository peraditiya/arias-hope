﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Viewport : MonoBehaviour {

		private Camera _camera;
		private Vector2 _cameraImp = Vector2.zero;
		private Vector3 _previousPosition;

		//public Rect bound;

		public Body body;

		public List<Parallax> parallax;

		public List<Background> background;

		public bool isXAxisLocked = false;
		public bool isYAxisLocked = false;

        public void Awake() {
			
			if(GetComponent<Camera>() == null) this._camera = Camera.main;
			else { this._camera = GetComponent<Camera>(); }

			this.body = new Body(this._camera.gameObject);
			this.parallax   = new List<Parallax>();
			this.background = new List<Background>();

			Root.viewport = this;
        }

        public void Start() {

            this.body.size = new Vector2((this._camera.orthographicSize * ((float)Screen.width / (float)Screen.height) ), this._camera.orthographicSize) * 2f;
            
			this._cameraImp = new Vector2(this.body.width/Stage.currentRoom.body.width, this.body.height/Stage.currentRoom.body.height);
        }

        public void Update() {

			Vector3 newCameraPosition = Root.player.unit.position;
			newCameraPosition.z = this.body.z;

            /*
            if (!this.isXAxisLocked) {

                if(newCameraPosition.x + this.body.widthH > Stage.currentRoom.body.right) newCameraPosition.x = Stage.currentRoom.body.right - this.body.widthH;
				else if(newCameraPosition.x - this.body.widthH < Stage.currentRoom.body.left) newCameraPosition.x = Stage.currentRoom.body.left + this.body.widthH;
                
            } else
				newCameraPosition.x = 0;

			if(!this.isYAxisLocked) {
			
				if(newCameraPosition.y + this.body.heightH > Stage.currentRoom.body.top) newCameraPosition.y = Stage.currentRoom.body.top - this.body.heightH;
				else if(newCameraPosition.y - this.body.heightH < Stage.currentRoom.body.bottom) newCameraPosition.y = Stage.currentRoom.body.bottom + this.body.heightH;
			
			} else
				newCameraPosition.y = 0;
            */

			this.body.position = newCameraPosition;
			this._previousPosition = newCameraPosition;
				
			if(this.parallax.Count > 0) {
				
				//Vector3 newParallaxPosition = newCameraPosition + new Vector3((newCameraPosition.x - Stage.currentRoom.body.left) * (-0.3f/Stage.currentRoom.hCell), (newCameraPosition.y - Stage.currentRoom.body.bottom) * (-0.1f/Stage.currentRoom.vCell), 0);
				
				foreach (Parallax p in this.parallax) {

                    /*
					Vector3 newParallaxPosition = new Vector3(
						(p.body.width/Stage.currentRoom.body.width)/(Stage.currentRoom.hCell * 1.25f), 
						(p.body.height/Stage.currentRoom.body.height)/(Stage.currentRoom.vCell + 1.25f), 0);

					//Debug.Log(Stage.currentRoom.l);
					newParallaxPosition.x = this.body.left - ((this.body.left - Stage.currentRoom.body.left) * newParallaxPosition.x) + p.body.widthH;
					newParallaxPosition.y = this.body.top + ((Stage.currentRoom.body.top - this.body.top) * newParallaxPosition.y) - p.body.heightH;
					
					newParallaxPosition.z = p.body.z;
					p.body.position = newParallaxPosition;	
				    */

                    Vector3 newParallaxPosition = new Vector3(
                            (p.body.width - this.body.width) / (Stage.currentRoom.body.width - this.body.width) * p.speed.x,
                            (p.body.height - this.body.height) / (Stage.currentRoom.body.height - this.body.height) * p.speed.y, 0);

                    //Debug.Log(p.body.name + " - " + newParallaxPosition);
                    //Debug.Log(this.body.left - Stage.currentRoom.body.left);
                    newParallaxPosition.x = this.body.left - ((this.body.left - Stage.currentRoom.body.left) * newParallaxPosition.x) + p.body.widthH;
                    newParallaxPosition.y = this.body.bottom - ((this.body.bottom - Stage.currentRoom.body.bottom) * newParallaxPosition.y) + p.body.heightH;

                    newParallaxPosition.z = p.body.z;
                    p.body.position = newParallaxPosition;
                }
			}

			if(this.background.Count > 0) {
				
				foreach (Background b in this.background) {

					b.body.position = new Vector3(newCameraPosition.x, newCameraPosition.y, b.body.z);
				}
			}
		}
	}
}